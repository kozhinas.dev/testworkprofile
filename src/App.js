import React, { Component } from 'react'
import Header from './components/Header'
import ProfileCard from './components/ProfileCard'
import EditCard from './components/EditCard'
import './App.css'


class App extends Component {
  state = {
    errorText: 'лохым таком',
    noEdit: false,
    editIcon: false,
    drawerIsOpen: false,
    dialogIsOpen: false,
    profileName: 'Иванова Анна Михайловна',
    fullName: '',
    email: '',
    phoneNumber: '',
    drawerSaveIsOpen: false,
    dialogSaveIsOpen: false

  }
  handleToggleDrawer = () => {
    this.setState({
      drawerIsOpen: !this.state.drawerIsOpen
    })
  }
  handleToggleDialog = () => {
    console.log('lox')
    this.setState({
      dialogIsOpen: !this.state.dialogIsOpen
    })
  }
  handleToggleDrawer = () => {
    this.setState({
      drawerIsOpen: !this.state.drawerIsOpen
    })
  }
  handleEdit = () => {
    this.setState({
      editIcon: !this.state.editIcon,
      noEdit: !this.state.noEdit
    })

  }
  handleTextFieldChangeFullName = (e) => {
    if (e.target.value.length > 2) {
      this.setState({ helperTextFullName: '', errorName: false, fullName: e.target.value });
    } else {
      this.setState({ helperTextFullName: 'Имя слишком короткое', errorName: true });
    }
  }
  handleTextFieldChangeEmail = (e) => {
    if (e.target.value.indexOf('@') != -1 && e.target.value.indexOf('.ru') != -1 || e.target.value.indexOf('.com') != -1 || e.target.value.indexOf('.net') != -1) {
      this.setState({ helperTextEmail: '', errorEmail: false, email: e.target.value });
    } else {
      this.setState({ helperTextEmail: 'Это не почта', errorEmail: true });
    }
  }
  handleTextFieldChangePhoneNumber = (e) => {
    if (e.target.value.length > 5) {
      this.setState({ helperTextPhoneNumber: '', errorPhone: false, phoneNumber: e.target.value });
    } else {
      this.setState({ helperTextPhoneNumber: 'Пароль слишком короткий', errorPhone: true });
    }
  }
  handleSave = () => {
    const { fullName, email, phoneNumber } = this.state;
    const data = Object.assign({'fullName': fullName}, {'email': email}, {'phoneNumber': phoneNumber});
    console.log(data)
    localStorage.setItem('fullName', fullName);
    localStorage.setItem('email', email);
    localStorage.setItem('phoneNumber', phoneNumber);
    fetch('http://jsonplaceholder.typicode.com/posts', {
        method: 'POST', 
        mode: 'cors',
        cache: 'no-cache', 
        credentials: 'same-origin', 
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': 'random'
        },
        redirect: 'follow', 
        referrerPolicy: 'no-referrer', 
        body: JSON.stringify(data) 
      });
    this.setState({
      profileName: this.state.fullName,
      drawerIsOpen: !this.state.drawerIsOpen,
      drawerSaveIsOpen: !this.state.drawerSaveIsOpen

    })
    setTimeout(() => {
      this.setState({drawerSaveIsOpen: !this.state.drawerSaveIsOpen});
    }, 1000)
  }
  handleSaveDialog = () => {
    const { fullName, email, phoneNumber } = this.state;
    const data = Object.assign({'fullName': fullName}, {'email': email}, {'phoneNumber': phoneNumber});
    console.log(data)
    localStorage.setItem('fullName', fullName);
    localStorage.setItem('email', email);
    localStorage.setItem('phoneNumber', phoneNumber);
    fetch('http://jsonplaceholder.typicode.com/posts', {
        method: 'POST', 
        mode: 'cors',
        cache: 'no-cache', 
        credentials: 'same-origin', 
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': 'random'
        },
        redirect: 'follow', 
        referrerPolicy: 'no-referrer', 
        body: JSON.stringify(data) 
      });
    this.setState({
      dialogIsOpen: !this.state.dialogIsOpen,
      profileName: this.state.fullName,
      drawerIsOpen: !this.state.drawerIsOpen,
      drawerSaveIsOpen: !this.state.drawerSaveIsOpen

    })
   
  }


    render() {
      
      const { noEdit, fullName, email, phoneNumber, dialogIsOpen, dialogSaveIsOpen, drawerSaveIsOpen, editIcon, profileName, drawerIsOpen,  errorName, errorPhone, errorEmail, helperTextFullName, helperTextEmail, helperTextPhoneNumber } = this.state
        return (
            <div className='header'>
                <Header
                profileName={profileName}
                handleEdit={this.handleEdit}
                editIcon={editIcon}/>
              <div className='main'>
              {noEdit ? <ProfileCard 
                          phoneNumber={phoneNumber}
                          email={email}/> 
                          : <EditCard 
                                handleTextFieldChangeFullName={this.handleTextFieldChangeFullName}
                                handleTextFieldChangeEmail={this.handleTextFieldChangeEmail}
                                handleTextFieldChangePhoneNumber={this.handleTextFieldChangePhoneNumber}
                                errorName={errorName}
                                errorPhone={errorPhone}
                                errorEmail={errorEmail}
                                helperTextFullName={helperTextFullName}
                                helperTextEmail={helperTextEmail}
                                helperTextPhoneNumber={helperTextPhoneNumber}
                                fullName={fullName}
                                handleToggleDrawer={this.handleToggleDrawer}
                                isOpen={drawerIsOpen}
                                dialogIsOpen={dialogIsOpen}
                                dialogSaveIsOpen={dialogSaveIsOpen}
                                drawerSaveIsOpen={drawerSaveIsOpen}
                                handleSaveDialog={this.handleSaveDialog}
                                handleToggleDialog={this.handleToggleDialog}
                                email={email}
                                phoneNumber={phoneNumber}
                                handleSave={this.handleSave}/> }
              </div>
            </div>
          
          
        )
    }
}

export default App

