import React from 'react'
import Card from '@material-ui/core/Card'
import Grid from '@material-ui/core/Grid'
import Drawer from './Drawer'
import FullNameField from './FullNameField'
import EmailField from './EmailField'
import PhoneField from './PhoneField'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import { createMuiTheme, ThemeProvider, useTheme } from '@material-ui/core/styles'
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd'
import Box from '@material-ui/core/Box'
import '../App.css'
import Email from '@material-ui/icons/AlternateEmail'
import Divider from '@material-ui/core/Divider'
import Notification from '@material-ui/icons/NotificationsNoneOutlined'

export default function ProfileCard(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('lg'));
  let editCard
  if (matches) {
      editCard = <><Grid container style={{height: '100%'}} justify='center' alignContent='center' alignItems='center'><Box display="flex" alignItems="center" justify="space-between" style={{width: '100%'}}>
        <Box flexGrow={0}>
          <AssignmentIndIcon style={{marginRight: '34px', color: '#00BFA5', width: '27px', height: '30px'}}/>
        </Box>
        <Box flexGrow={1}>
          <FullNameField style={{marginTop: '49px'}} data={props}/>
        </Box>
        <Box flexGrow={0}>
          <Divider style={{background: '#CAE7FE', height: '97px'}} orientation='vertical' flexItem variant='inset'/>
        </Box>
        <Box flexGrow={0}>
          <Email style={{marginLeft: '32px', marginRight: '35px', color: '#00BFA5', width: '30px', height: '30px'}}/>
        </Box>
        <Box flexGrow={1}>
          <EmailField data={props}/>
        </Box>
        <Box flexGrow={0}>
          <Divider style={{background: '#CAE7FE', height: '97px'}} orientation='vertical' flexItem variant='inset'/>
        </Box>
        <Box flexGrow={0}>
          <Notification style={{marginLeft: '32px', marginRight: '35px', color: '#00BFA5', width: '27px', height: '27px'}}/>
        </Box>
        <Box flexGrow={1}>
          <PhoneField data={props}/>
        </Box>
        </Box>
    <Box flexGrow={0}>
      <Drawer  onClick={props.handleToggleDrawer} disabled={props.fullName.length == 0  || props.email.length == 0 || props.phoneNumber.length == 0} data={props} handleSave={props.handleSave} handleToggleDrawer={props.handleToggleDrawer}/>
      </Box></Grid></>
      
    } else {
      editCard =  <><Grid container style={{height: '100%'}} justify='center' alignContent='space-between'>

        <FullNameField data={props}/>
        <EmailField data={props}/>
        <PhoneField data={props}/>
        <Drawer onClick={props.handleToggleDrawer} disabled={props.fullName.length == 0  || props.email.length == 0 || props.phoneNumber.length == 0} data={props} handleSave={props.handleSave} handleToggleDrawer={props.handleToggleDrawer}/>
    </Grid></>
    
    } 
  return (
    <Card style={{borderRadius: '10px', padding: '26px 23px 17px 23px', boxShadow: '0px 4px 10px rgba(0, 0, 0, 0.15)', height: '245px'}}>
      
      {editCard}

  </Card>
  )
}

