import React from 'react'
import Avatar from '@material-ui/core/Avatar'
import avatar from '../static/images/avatar/avatar.png'
import Notification from '@material-ui/icons/NotificationsNoneOutlined'
import Divider from '@material-ui/core/Divider'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import EditIcon from '@material-ui/icons/Edit'
import ClearIcon from '@material-ui/icons/Clear'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import { createMuiTheme, ThemeProvider, useTheme } from '@material-ui/core/styles';


export default function Header(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('lg'));
  let button
  if (props.editIcon && matches) {
      button = <>
                <Box flexGrow={1}>
                 <Typography className='h3' variant='body1' onClick={props.handleEdit}>Редактировать</Typography>
                </Box>
                <Box flexGrow={0}>
                  <EditIcon style={{ color: 'white', width: '18px', height: '18px'}} onClick={props.handleEdit}/>
                </Box>
              </>
    } else if(!props.editIcon && matches){
      button =  <>
               <Box flexGrow={1}>
                <Typography className='h3' variant='body1' onClick={props.handleEdit}>Закрыть</Typography>
               </Box>
               <Box flexGrow={0}>
                  <ClearIcon style={{ color: 'white', width: '18px', height: '18px'}} onClick={props.handleEdit}/>
                </Box>
                </>
    } else if(!props.editIcon) {
      button = <Box flexGrow={1}>
                 <ClearIcon style={{ color: 'white', width: '18px', height: '18px'}} onClick={props.handleEdit}/>
                </Box>
    } else if(props.editIcon) {
        button = <Box flexGrow={1}>
                    <EditIcon style={{ color: 'white', width: '18px', height: '18px'}} onClick={props.handleEdit}/>
                  </Box>
    }   
  return (
    <div>
      <Box display="flex" alignItems="center">
        <Box flexGrow={1}>
          <Notification style={{ color: 'white', fontSize: '16px'}} />
        </Box>
        <Box flexGrow={0}>
          <Divider style={{background: 'white', height: '15px'}} orientation='vertical' flexItem variant='middle'/>
        </Box>
        <Box flexGrow={0}>
          <Avatar alt='Remy Sharp' src={avatar} style={{width: '16px', height: '16px', marginRight: '5px'}}/>
        </Box>
      
        {matches && <Typography className='h3' variant='body1'>{props.profileName.substr(0, props.profileName.indexOf(' ') + 2) + '.'}</Typography>}
      </Box>
      <Typography className='h1' variant='body1'>личный профиль</Typography>
      <Typography className='h1 h2' variant='body2'>Главная/Личный профиль</Typography>
      <Box color='text.disabled' className='box1' display="flex" alignItems="center">
              <Box flexGrow={0}>
                <Avatar alt='Remy Sharp' src={avatar} style={{width: '40px', height: '40px', marginRight: '5px'}}/>
              </Box>
              <Box flexGrow={0}>
                <Typography  className='h3' variant='body1'> {props.profileName}</Typography>
              </Box>
              {button}
              </Box>
            
    </div>
  )
}






