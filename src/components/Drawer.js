import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer'
import Button from '@material-ui/core/Button'
import ClearIcon from '@material-ui/icons/Clear'
import Typography from '@material-ui/core/Typography'
import { createMuiTheme, ThemeProvider, useTheme } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import Grid from '@material-ui/core/Grid'
import Dialog from '@material-ui/core/Dialog'

const useStyles = makeStyles({
  drawerPaper: {
    borderRadius: '20px 20px 0 0',
  },
})
export default function Drawer(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('lg'));
  const classes = useStyles()
  return (
    <div>
    {matches ? <><Button disabled={props.disabled} onClick={props.data.handleToggleDialog} style={{width: '100%', margin: '0px 21px', borderRadius: '36px', height: '49px', backgroundColor: '#01BDA7', color: 'white', textTransform: 'none'}} variant="contained" href="#contained-buttons">Сохранить изменения</Button>
      <Dialog onClose={props.handleToggleDrawer} aria-labelledby="customized-dialog-title" open={props.data.dialogIsOpen}>
        <Grid container justify='flex-end'>
          <ClearIcon onClick={props.data.handleToggleDialog} style={{padding: '25px 25px 25px 0px', textAlign: 'right'}}>Закрыть</ClearIcon>
        </Grid>
        <Typography style={{fontSize: '18px', textAlign: 'center'}} variant='body1'>Сохранить изменения?</Typography>
        <Button onClick={props.data.handleSaveDialog} style={{ borderRadius: '36px', height: '50px', margin: '40px 199px 0 199px', width: '200px', backgroundColor: '#01BDA7', color: 'white', textTransform: 'none'}} variant="contained" href="#contained-buttons">Сохранить</Button>
        <Button onClick={props.data.handleToggleDialog} style={{ borderRadius: '36px', height: '50px', margin: '28px 199px 56px 199px', width: '200px', backgroundColor: '#01BDA7', color: 'white', textTransform: 'none'}} variant="contained" href="#contained-buttons">Не cохранять</Button>
      </Dialog>
      <Dialog onClose={props.handleToggleDrawer} aria-labelledby="customized-dialog-title" open={props.data.isOpen}>
        <Typography style={{fontSize: '18px', textAlign: 'center', padding: '29px 0'}} variant='body1'>Данные успешно сохранены</Typography>
        <Button onClick={props.handleToggleDrawer} style={{ borderRadius: '36px', height: '50px', margin: '28px 199px 56px 199px', width: '200px', backgroundColor: '#01BDA7', color: 'white', textTransform: 'none'}} variant="contained" href="#contained-buttons">Хорошо</Button>
      </Dialog>
      </> :  <><Button disabled={props.disabled} onClick={props.handleToggleDrawer} style={{width: '100%', margin: '0px 21px', borderRadius: '36px', height: '49px', backgroundColor: '#01BDA7', color: 'white', textTransform: 'none'}} variant="contained" href="#contained-buttons">Cохранить изменения</Button>
      <SwipeableDrawer
        anchor="bottom"
        open={props.data.isOpen}
        onClose={props.handleToggleDrawer}
        onOpen={props.handleToggleDrawer}
        classes={{ paper: classes.drawerPaper }}
      >
     <Grid container justify='flex-end'>
        <ClearIcon onClick={props.handleToggleDrawer} style={{padding: '25px 25px 25px 0px', textAlign: 'right'}}>Закрыть</ClearIcon>
      </Grid>
        <Typography style={{fontSize: '18px', textAlign: 'center'}} variant='body1'>Сохранить изменения?</Typography>
        <Button onClick={props.handleSave} style={{padding: '16px 63px 15px 62px', borderRadius: '36px', height: '50px', margin: '40px 60px 0 58px', backgroundColor: '#01BDA7', color: 'white', textTransform: 'none'}} variant="contained" href="#contained-buttons">Сохранить</Button>
        <Button onClick={props.handleToggleDrawer} style={{padding: '16px 53px 15px 54px', borderRadius: '36px', height: '50px', margin: '28px 60px 168px 58px', backgroundColor: '#01BDA7', color: 'white', textTransform: 'none'}} variant="contained" href="#contained-buttons">Не cохранять</Button>
      </SwipeableDrawer>
      <SwipeableDrawer
        anchor="bottom"
        open={props.data.drawerSaveIsOpen}
        onClose={props.handleToggleDrawer}
        onOpen={props.handleToggleDrawer}
        classes={{ paper: classes.drawerPaper }}
      >
        <Typography style={{fontSize: '18px', textAlign: 'center', padding: '29px 0'}} variant='body1'>Данные успешно сохранены</Typography>
      </SwipeableDrawer></>}
    </div>
  )
}