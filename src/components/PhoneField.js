import React from 'react'
import TextField from '@material-ui/core/TextField'
import '../App.css'

export default function PhoneField(props) {
  return (
    <TextField
    helperText={props.data.helperTextPhone}
    className={props.className}
    error={props.data.errorPhone}
    onChange={props.data.handleTextFieldChangePhoneNumber.bind(this)}
    id="outlined-number"
    label="Номер телефона"
    type="number"
    InputLabelProps={{
      shrink: true,
    }}
    variant="outlined"
    fullWidth='true'
    placeholder='Укажите номер телефона'
  />
  )
}
 


