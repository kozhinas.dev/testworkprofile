import React from 'react'
import TextField from '@material-ui/core/TextField'


export default function EmailField(props) {
  return (
    <TextField
          helperText={props.data.helperTextEmail}
          className={props.className}
          onChange={props.data.handleTextFieldChangeEmail.bind(this)}
          error={props.data.errorEmail}
          InputLabelProps={{
            shrink: true,
          }}
          id="outlined-Email"
          placeholder="Ivanova@mail.ru"
          label="E-mail"
          fullWidth='true'
          variant="outlined"
        />  
  )
}
 