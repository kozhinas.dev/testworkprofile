import React from 'react'
import TextField from '@material-ui/core/TextField'

export default function FullNameField(props) {
  return (
    <TextField
          helperText={props.data.helperTextFullName}
          className={props.className}
          onChange={props.data.handleTextFieldChangeFullName.bind(this)}
          error={props.data.errorName}
          InputLabelProps={{
            shrink: true,
          }}
          id="outlined-fullname"
          placeholder="Укажите ваши фамилию и имя"
          label="Фамилия и имя"
          fullWidth='true'
          variant="outlined"
        />  
  )
}
 






