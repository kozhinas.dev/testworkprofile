import React from 'react'
import Card from '@material-ui/core/Card'
import Grid from '@material-ui/core/Grid'
import Email from '@material-ui/icons/AlternateEmail'
import Typography from '@material-ui/core/Typography'
import CallIcon from '@material-ui/icons/Call'
import Divider from '@material-ui/core/Divider'

export default function ProfileCard(props) {
  return (
    <Card style={{borderRadius: '10px', height: '128px', boxShadow: '0px 4px 10px rgba(0, 0, 0, 0.15)', width: '100%'}}>
      <Grid container justify='center' style={{height: '100%', width: '100%'}}>
         <Grid container style={{paddingLeft: '12px', height: '49%'}} alignContent='center'>
            <Email style={{color: '#00BFA5'}}/>
            <Typography className='form' variant='body1'>{props.email || 'youremail@mail.ru'}</Typography>
         </Grid>
         <Divider style={{background: '#CAE7FE', width: '100%'}} orientation='horizontal' variant='middle'/>
          <Grid container style={{paddingLeft: '12px', height: '49%'}} alignContent='center'>
            <CallIcon style={{color: '#00BFA5'}}/>
            <Typography className='form' variant='body1'>{props.phoneNumber || 'Укажите номер телефона'}</Typography>
         </Grid>
      </Grid>
    </Card> 
  )
}






